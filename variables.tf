variable "bastion" {
  default = false
}

variable "username" {
  default = "fwadmin"
}

variable "ssh_key" { }

variable "location" {}
variable "rg-name" {}
variable "subnet_id" {}
variable "name" {}

